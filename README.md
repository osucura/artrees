# ARTrees

This repository is intended to capture code and other controlled files of historical interest from the ARTrees project.  Recent developments for that project as of February 2020 have rendered this work obsolete.  For more details, including documentation of the files herein, refer to Basecamp (specifically https://3.basecamp.com/3904773/buckets/7945656/messages/2427099658)

Note that some of the files of this repository (specifically in server_config and api) include content that is not relevant to ARTrees, but was included to preserve the entirety of the file.
