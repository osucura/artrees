var http = require('http');
var url = require('url');
geojson = require('/home/node/lib/node_modules/geojson');
tsv = require('/home/node/lib/node_modules/tsv');

var mongodb = require('/home/node/lib/node_modules/mongodb');
var HttpDispatcher = require('/home/node/lib/node_modules/httpdispatcher');
const PORT=9090;
var MongoClient = mongodb.MongoClient;
var busesurl = 'mongodb://localhost:27017/buses';
var parkingurl = 'mongodb://localhost:27017/parking';
var cotaurl = 'mongodb://localhost:27017/cota';

var poarkingdb;
var parkingcoll;
var busesdb;
var buscoll;
var cotadb;
var cotaschedulecoll;

MongoClient.connect(parkingurl, function(err, db){
	if(err) throw err;
	parkingcoll = db.collection('usage');
        });

MongoClient.connect(busesurl,  function(err, db){
	if(err) throw err;
	buscoll = db.collection('buscoll');
	});        

MongoClient.connect(cotaurl, function(err, db){
	if(err) throw err;
	cotaschedulecoll = db.collection('cotaschedule');
	});

const pginit = {
  error: (error, e) => {
    if (e.cn) {
        console.log('CN:',e.cn);
        console.log('EVENT', error.message || error);
    }
    }
};

var pgp = require('/home/node/lib/node_modules/pg-promise')(pginit);
var airdb = pgp('postgresql://postgres:postgres.com@localhost/airquality');
airdb.connect();  


var ardb = pgp('postgresql://postgres:postgres.com@localhost/ar');
ardb.connect();

var dispatcher = new HttpDispatcher();
function handleRequest(request, response){
    try {
        //log the request on console
        //console.log(request.url);
        //Disptach
        dispatcher.dispatch(request, response);
    } catch(err) {
        console.log(err);
    }
}

//For all your static (js/css/images/etc.) set the directory name (relative path).
dispatcher.setStatic('resources');

dispatcher.onGet("/page1", function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Page One');
});    
  
dispatcher.onGet("/buses_geojson", function(req, res) {
	res.writeHead(200, {"Content-Type": "application/json"});
        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                       || req.connection.remoteAddress;
	console.log(ip);
	timestamp = Math.floor(Date.now()/1000);

	var qs = url.parse(req.url, true).query;
	if (qs.start != null && qs.end != null){
		starttime = parseInt(qs.start);
		endtime = parseInt(qs.end);
     		mqs = {$gte: starttime, $lte: endtime};
		console.log(JSON.stringify(mqs));

	} else if (qs.start != null && qs.end == null){
		starttime = parseInt(qs.start);
		mqs = {$gte: starttime};
		console.log(JSON.stringify(mqs));

	} else if (qs.start == null && qs.end != null){
		endtime = parseInt(qs.end);
		mqs = {$lte: endtime};
		console.log(JSON.stringify(mqs));

	} else {
		timesel = timestamp - 60;
		mqs = {$gte: timesel};
	}

	if (qs.limit != null){
		newlimit = parseInt(qs.limit);
	}
	else {
		newlimit = 0;
	}

	

	buscoll.find({"timestamp": mqs}, {_id:0 }).sort({"trip_id":1}).limit(newlimit).toArray(function (err, result) {
		  if (err) {
			console.log(err);
		  } else if (result.length) {
			
			var returnjson = result;
		        
			parsedgeojson = geojson.parse(returnjson, {Point: ['veh_pos_lat','veh_pos_lon']});
			res.end(JSON.stringify(parsedgeojson));
			
			}	
		});	  
	})
  
dispatcher.onGet("/parking_json", function(req, res) {
	res.writeHead(200, {"Content-Type": "application/json"});
	timestamp = Math.floor(Date.now()/1000);
        //lastweek = timestamp - 604800;
        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);

	var timesel = 0;
 	//trying to get querystring
     	var qs = url.parse(req.url, true).query;

	if (qs.start != null && qs.end != null){
                starttime = parseInt(qs.start);
                endtime = parseInt(qs.end);
                mqs = {$gte: starttime, $lte: endtime};
                console.log(JSON.stringify(mqs));

        } else if (qs.start != null && qs.end == null){
                starttime = parseInt(qs.start);
                mqs = {$gte: starttime};
                console.log(JSON.stringify(mqs));

        } else if (qs.start == null && qs.end != null){
                endtime = parseInt(qs.end);
                mqs = {$lte: endtime};
                console.log(JSON.stringify(mqs));

        } else {
                timesel = timestamp - 604800;
                mqs = {$gte: timesel};
        }

        if (qs.limit != null){
                newlimit = parseInt(qs.limit);
        }
        else {
                newlimit = 0;
        }

       


	//console.log(qs.time);
	//if (qs.time != null){
	//	timesel = parseInt(qs.time);

	//} else {
	//	timesel = timestamp - 604800;
	//};

		
	parkingcoll.find({"ts": mqs}, {_id:0,  "GarageUrl":0,"GarageType":0,"GarageId":0,"OSUID":0 }).limit(newlimit).toArray(function (err, result) {
		if (err) {
			console.log(err);
		  } 
		else if (result.length) {
			var returnjson = result
			res.end(JSON.stringify(returnjson));
			//res.end(returnjson);

		  } else {
			console.log('No document(s) found with defined "find" criteria!');
			res.end("no data returned");
		 }
	});
});  

dispatcher.onGet("/cogobikes", function(req, res) {
    //var adr = "http://geog-gis.asc.ohio-state.edu/cogo-query/get-cogo.php";
    //var adr = "http://164.107.176.32/cogo-query/get-cogo.php";
    var adr = "http://128.146.194.7/misc/get-cogo.php";
    http.get(adr, res1 => {
        var body='';
        res1.on('data', data => {
            body += data;
        });
        res1.on('end', ()=> {
            res.end(body.toString());
        });
    });
});

/*
dispatcher.onGet("/cogobikes", function(req, res) {
    var adr = "http://128.146.194.7/misc/cogo/get-cogo.php";
    //var adr = "http://164.107.176.32/cogo-query/get-cogo.php";
    http.get(adr, res1 => {
	var body='';
	res1.on('data', data => {
	    body += data;
	});
	res1.on('end', ()=> {
	    res.end(body.toString());
	});
    });
});
*/

dispatcher.onGet("/get-trip-info-all", function(req, res) {
    var adr = "http://128.146.194.7/misc/trips/get-trip-info-all.php";
    http.get(adr, res1 => {
        var body='';
        res1.on('data', data => {
            body += data;
        });
        res1.on('end', ()=> {
            res.end(body.toString());
        });
    });
});

dispatcher.onGet("/get-cotweets-themes5", function(req, res) {
    //var adr = "http://128.146.194.7/misc/twitter/get-cotweets-themes5.php?days=30";
    var adr = "http://164.107.176.32/tweets-query/get-cotweets-themes5.php?days=30";
    http.get(adr, res1 => {
        var body='';
        res1.on('data', data => {
            body += data;
        });
        res1.on('end', ()=> {
            res.end(body.toString());
        });
    });
});


dispatcher.onGet("/airquality", function(req, res) {
    //var adr = "http://128.146.194.7/misc/get-airquality.php";
    var adr = "https://geog-gis.asc.ohio-state.edu/airquality/query_airquality_psql.php";
    //var adr = "http://164.107.176.32/airquality/query_airquality_psql.php";
    http.get(adr, res1 => {
	var body='';
	res1.on('data', data => {
	    body += data;
	});
	res1.on('end', ()=> {
	    res.end(body.toString());
	});
    });
});

dispatcher.onGet("/airquality1", function(req, res){
        res.writeHead(200, {"Content-Type": "text"});

	//console.log(req);
        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);

	airdb.any("SELECT date,aqi,sitename as site,lat,lng FROM data where parameter='PM2.5' and date between now()-cast('24 hour' AS interval) and now();", [true])
	.then(function(data) {
	    //x = JSON.stringify(data.toString());
	    //res.end("it probably won't work...");
	    res.end(tsv.stringify(data));
	}) 
	.catch(function(error) {
       	    console.log("damn");
	    res.end('well, damn');
	})  


});

dispatcher.onGet("/trees", function(req, res){
        res.writeHead(200, {"Content-Type": "application/json"});

        var pcs = '3857';
        var gcs = '4326'; 
        var usepcs = false;

        var qs = url.parse(req.url, true).query;
        x = parseFloat(qs.x);
        y = parseFloat(qs.y);
        dist = parseFloat(qs.dist);
        limit = parseFloat(qs.limit);
	if ( "usepcs" in qs ){
	   if ( qs.usepcs == 1 ){
	     usepcs = true;
	   } else {
	     usepcs = false;
           }
	};

        //qs = lon+","+lat+","+dist+","+limit;

 
        //res.end(qs);

	if( usepcs ){
	    qs = "select ST_X(ST_Transform(t.the_geom,"+pcs+")),ST_Y(ST_Transform(t.the_geom,"+pcs+")),genus,primarycom,plantsize,treeid,genuscode,specificep,plantcondi, ST_Distance(t.the_geom::geography, ST_Transform(ST_GeomFromText('POINT("+x+' '+y+")',"+pcs+"),"+gcs+")::geography) as dist,degrees(ST_Azimuth(ST_Transform(ST_SetSRID(ST_Point("+x+','+y+"),"+pcs+"),"+gcs+"), t.the_geom)) as angle from trees_wgs84_s t WHERE ST_DWithin(t.the_geom::geography, ST_Transform(ST_GeomFromText('POINT("+x+' '+y+")',"+pcs+"),"+gcs+")::geography,"+dist+") order by dist limit "+limit+";"
	} else {	 
	    qs = "select ST_X(t.the_geom),ST_Y(t.the_geom),genus,primarycom,plantsize,treeid,genuscode,specificep,plantcondi, ST_Distance(t.the_geom::geography, ST_GeomFromText('POINT("+x+' '+y+")',"+gcs+")::geography) as dist,degrees(ST_Azimuth(ST_SetSRID(ST_Point("+x+','+y+"),"+gcs+"), t.the_geom)) as angle from trees_wgs84_s t WHERE ST_DWithin(t.the_geom::geography, ST_GeomFromText('POINT("+x+' '+y+")',"+gcs+")::geography,"+dist+") order by dist limit "+limit+";"
	}
       //res.end(qs);


        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);

        ardb.any(qs, [true])
       .then(function(data) {
            //x = JSON.stringify(data);
            var treedata = data;
	   if ( usepcs ) {
               parsedgeojson = geojson.parse(treedata, {
		   Point: ['st_y','st_x'],
		   "crs": {
		       "type": "name",
		       "properties": {
			   "name": "EPSG:"+pcs
		       }
		   }
	       });

	   } else {
               parsedgeojson = geojson.parse(treedata, {
		   Point: ['st_y','st_x'],
		   "crs": {
		       "type": "name",
		       "properties": {
			   "name": "EPSG:"+gcs
		       }
		   }
	       });
	   }
            res.end(JSON.stringify(parsedgeojson));

            //res.end("it probably won't work...");
            //res.end(tsv.stringify(data));
           // res.end(data);
           // res.end(x);
        })
        .catch(function(error) {
            console.log("damn");
            res.end('well, damn');
        })

});


dispatcher.onGet("/futuretrees", function(req, res){
        res.writeHead(200, {"Content-Type": "application/json"});

        var pcs = '3857';
        var gcs = '4326'; 
        var usepcs = false;

        var qs = url.parse(req.url, true).query;
        x = parseFloat(qs.x);
        y = parseFloat(qs.y);
        dist = parseFloat(qs.dist);
        limit = parseFloat(qs.limit);
	if ( "usepcs" in qs ){
	   if ( qs.usepcs == 1 ){
	     usepcs = true;
	   } else {
	     usepcs = false;
           }
	};

	if( usepcs ){
	    qs = "select ST_X(ST_Transform(t.wkb_geometry,"+pcs+")),ST_Y(ST_Transform(t.wkb_geometry,"+pcs+")),rank, ST_Distance(t.wkb_geometry::geography, ST_Transform(ST_GeomFromText('POINT("+x+' '+y+")',"+pcs+"),"+gcs+")::geography) as dist,degrees(ST_Azimuth(ST_Transform(ST_SetSRID(ST_Point("+x+','+y+"),"+pcs+"),"+gcs+"), t.wkb_geometry)) as angle from futuretrees t WHERE ST_DWithin(t.wkb_geometry::geography, ST_Transform(ST_GeomFromText('POINT("+x+' '+y+")',"+pcs+"),"+gcs+")::geography,"+dist+") order by dist limit "+limit+";"
	    console.log(qs)
	} else {	 
	    qs = "select ST_X(t.wkb_geometry),ST_Y(t.wkb_geometry),rank, ST_Distance(t.wkb_geometry::geography, ST_GeomFromText('POINT("+x+' '+y+")',"+gcs+")::geography) as dist,degrees(ST_Azimuth(ST_SetSRID(ST_Point("+x+','+y+"),"+gcs+"), t.wkb_geometry)) as angle from futuretrees t WHERE ST_DWithin(t.wkb_geometry::geography, ST_GeomFromText('POINT("+x+' '+y+")',"+gcs+")::geography,"+dist+") order by dist limit "+limit+";"
	    console.log(qs)
	}

        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);

        ardb.any(qs, [true])
       .then(function(data) {
           var treedata = data;
	   if ( usepcs ) {
               parsedgeojson = geojson.parse(treedata, {
		   Point: ['st_y','st_x'],
		   "crs": {
		       "type": "name",
		       "properties": {
			   "name": "EPSG:"+pcs
		       }
		   }
	       });

	   } else {
               parsedgeojson = geojson.parse(treedata, {
		   Point: ['st_y','st_x'],
		   "crs": {
		       "type": "name",
		       "properties": {
			   "name": "EPSG:"+gcs
		       }
		   }
	       });
	   }
            res.end(JSON.stringify(parsedgeojson));
        })
        .catch(function(error) {
            console.log("damn");
            res.end('well, damn');
        })

});

dispatcher.onGet("/cotaupdates", function(req, res){
        res.writeHead(200, {"Content-Type": "application/json"});

        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);

	var GtfsRealtimeBindings = require('gtfs-realtime-bindings');
        var request = require('request');
 
	var requestSettings = {
  		method: 'GET',
  		url: 'http://realtime.cota.com/TMGTFSRealTimeWebService/TripUpdate/TripUpdates.pb',
  		encoding: null
	};
  	cotaarray = []
	request(requestSettings, function (error, response, body) {
  		if (!error && response.statusCode == 200) {
    		var feed = GtfsRealtimeBindings.FeedMessage.decode(body);
    		feed.entity.forEach(function(entity) {
      			if (entity.trip_update) {
        			//console.log(entity.trip_update);
				cotaarray.push(entity.trip_update);
      			}
		//res.end(JSON.stringify(cotaarray));
    		});
		res.end(JSON.stringify(cotaarray));
  	}		
       }); 
});


dispatcher.onGet("/cotarealtime", function(req, res){
    res.writeHead(200, {"Content-Type": "application/json"});


	//console.log(req);
        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);




    var GtfsRealtimeBindings = require('gtfs-realtime-bindings');
    var request = require('request');
 
	var requestSettings = {
  		method: 'GET',
  		url: 'http://realtime.cota.com/TMGTFSRealTimeWebService/Vehicle/VehiclePositions.pb',
  		encoding: null
	};
  	cotaarray = []
	request(requestSettings, function (error, response, body) {
  		if (!error && response.statusCode == 200) {
    		var feed = GtfsRealtimeBindings.FeedMessage.decode(body);
    		feed.entity.forEach(function(entity) {
      			if (entity.trip_update) {
        			//console.log(entity.trip_update);
				console.log(entity);
				//cotaarray.push(entity.trip_update);
      			}
		res.end(JSON.stringify(cotaarray));
    		});
  	}
       }); 
});


dispatcher.onGet("/tweets", function(req, res){
    var adr = "http://128.146.194.7/misc/get-tweets.php";
    //var adr = "http://164.107.176.32/tweets-query/query-times-sum.php?maxhours=24&precision=minute";
    http.get(adr, res1 => {
	var body='';
	res1.on('data', data => {
	    body += data;
	});
	res1.on('end', ()=> {
	    res.end(body.toString());
	});
    });

});

dispatcher.onGet("/trips", function(req, res){
    var adr = "http://128.146.194.7/misc/trips/get-trip-info2.php";
    http.get(adr, res1 => {
	var body='';
	res1.on('data', data => {
	    body += data;
	});
	res1.on('end', ()=> {
	    res.end(body.toString());
	});
    });

});

dispatcher.onGet("/get_trip_info_one", function(req, res) {
    var adr = "http://128.146.194.7/misc/trips/get-trip-info-one.php";
    http.get(adr, res1 => {
        var body='';
        res1.on('data', data => {
            body += data;
        });
        res1.on('end', ()=> {
            res.end(body.toString());
        });
    });
});


dispatcher.onGet("/cotaschedule", function(req, res) {
        res.writeHead(200, {"Content-Type": "application/json"});
        var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                       || req.connection.remoteAddress;
        console.log(ip);
        timestamp = Math.floor(Date.now()/1000);

        var qs = url.parse(req.url, true).query;
        if (qs.tripid != null){
                
		var today = new Date();
		var todaydate = new Date(today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate());
		console.log(todaydate);
		var datetime = todaydate.getTime();
		var timediff = timestamp - (datetime/1000)-(4*3600);                

		console.log(timediff);

		tripidnum = parseInt(qs.tripid);


		mqs = {$and: [{"tripid": tripidnum},{"dayseconds": {$gte: timediff}}]}

		//$gte: timediff};
                console.log(mqs);
        } else {
		console.log("right before mqs generated");
		 mqs = {"$and": [{"tripid": 604997},{"dayseconds": {$gte: 10000}}]};
		res.end({"warning":"sorry bub..."});                
        }

	//{"$and":[{"tripid":"609226"},{"dayseconds":{"$gte":66112}}]}

        cotaschedulecoll.find(mqs, {_id:0 }).sort({"stopseq":1}).toArray(function (err, result) {
                  if (err) {
                        console.log(err);
                  } else {
			//if (result.length) {
                         var returnjson = result;

                         res.end(JSON.stringify(returnjson));
                        }
                });
        })

//yp addition
var db_url = 'mongodb://localhost:27017';

var db_cota_curio;
var db_cota_curio_fixedterm;
var db_cota_curio_dailytrips;
var col_stops;
var col_arcs;
var col_lines;
var col_joinedfeeds;

MongoClient.connect(db_url, function(err, client){
	if(err) throw err;

	db_cota_curio = client.db('cota_curio');
	db_cota_curio_fixedterm = client.db('cota_curio_fixedterm');
	db_cota_curio_dailytrips = client.db('cota_curio_dailytrips');
	col_stops = db_cota_curio.collection('dp_stops');
    	col_arcs = db_cota_curio.collection('dp_arcs');
	col_lines = db_cota_curio.collection('dp_lines');
	col_joinedfeeds = db_cota_curio.collection('dp_joinedfeeds');
});

 
dispatcher.onGet("/cotastops", function(req, res) {

	res.writeHead(200, {"Content-Type": "application/json"});
    var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                    || req.connection.remoteAddress;
	console.log(ip);

	col_stops.find().toArray(function (err, result) {
		  if (err) {
			console.log(err);
		  } else if (result.length) {
			var returnjson = result;
			parsedgeojson = geojson.parse(returnjson, {Point: 'coords', exclude:['_id']});
			res.end(JSON.stringify(parsedgeojson));
			}	
	});	  
});

dispatcher.onGet("/cotaarcs", function(req, res) {

	res.writeHead(200, {"Content-Type": "application/json"});
    var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                    || req.connection.remoteAddress;
	console.log(ip);

	col_arcs.find().toArray(function (err, result) {
		  if (err) {
			console.log(err);
		  } else if (result.length) {
			var returnjson = result;
			parsedgeojson = geojson.parse(returnjson, {LineString: 'coords', exclude:['_id']});
			res.end(JSON.stringify(parsedgeojson));
			}	
	});	  
});

dispatcher.onGet("/cotalines", function(req, res) {

	res.writeHead(200, {"Content-Type": "application/json"});
    var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                    || req.connection.remoteAddress;
	console.log(ip);

	col_lines.find().toArray(function (err, result) {
		  if (err) {
			console.log(err);
		  } else if (result.length) {
			var returnjson = result;
			parsedgeojson = geojson.parse(returnjson, {LineString: 'coords', exclude:['_id']});
			res.end(JSON.stringify(parsedgeojson));
			}	
	});	  
});


dispatcher.onGet("/cotafeeds", function(req, res) {

	res.writeHead(200, {"Content-Type": "text/plain"});
    	var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                    || req.connection.remoteAddress;
	console.log(ip);

	var timespan = 24*60; //unit: minute
	var timeresolution = 30; //unit: minute
	var ctimestamp = Math.floor(Math.floor(new Date().getTime()/1000)/60)*60 - 5*60;
	var feeds = [];
	var query = [];
    	var timegroups = {};

	for (var i=0; i<timespan/timeresolution;i++){
		var ts_gte = ctimestamp - timeresolution*60*i
		var time_gte = new Date(ts_gte*1000);
		time_gte.setTime(time_gte.getTime() - time_gte.getTimezoneOffset()*60*1000)
		var time_lt = new Date(ts_gte*1000 + 1*60*1000);
		time_lt.setTime(time_lt.getTime() - time_lt.getTimezoneOffset()*60*1000)
        	query.push({"localtime":{$gte:time_gte, $lt:time_lt}})
        	timegroups[ts_gte] = 0;
    	};
    	query.sort(function(a,b) {return (a["localtime"]["$gte"] > b["localtime"]["$gte"] ) ? 1 : ((b["localtime"]["$gte"]  > a["localtime"]["$gte"] ) ? -1 : 0);} ); 
    
	col_joinedfeeds.find({$or:query}).toArray(function (err, result) {
		if (err) {
			console.log(err);
		} else if (result.length){
			for (let i of result){
                		var feature = i;
                		var key = Math.floor(i.timestamp/60)*60;
                		delete feature['_id'];
                		delete feature['bearing'];
                		delete feature['vehicle_label'];
                		delete feature['localtime'];
				feature['timegroup'] = key;
                		feeds.push(feature);
                		timegroups[key] += 1;
            		};
            
            		var dummy_feature = JSON.parse(JSON.stringify(feature));
            		for (let i of Object.keys(dummy_feature)){
            			dummy_feature[i] = null;
            		}

            		for (let i of Object.keys(timegroups)){
                		if (timegroups[i] == 0){
                    			dummy_feature = JSON.parse(JSON.stringify(dummy_feature));
                    			dummy_feature["timegroup"] = i;
                    			feeds.push(dummy_feature)
                		}
            		}
			res.end(tsv.stringify(feeds));
		}
	});	
});

dispatcher.onGet("/cotafeeds_animation", function(req, res) {
	res.writeHead(200, {"Content-Type": "text/plain"});
    	var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                    || req.connection.remoteAddress;
	console.log(ip);

	var timespan = 6*60; //time span for animation - unit: minute
	var ctimestamp;
	var qs = url.parse(req.url, true).query;
	if (qs.start != null){
		ctimestamp = parseInt(qs.start)/1000; //in sec
	} else {
		ctimestamp = (new Date().getTime() - 5*60*1000)/1000; //in sec
	}

	var cdatetime = new Date(ctimestamp*1000);
	var mtimestamp = new Date(cdatetime.getFullYear(), cdatetime.getMonth(), cdatetime.getDate()).getTime()/1000; //in sec

	var col_timestamp;
	for (i=0; i<5; i++){
		if (ctimestamp >= mtimestamp + i*timespan*60 && ctimestamp < mtimestamp + (i+1)*timespan*60){
			col_timestamp = parseInt(mtimestamp + i*timespan*60);
			break;
		}
    	};
    
	var feeds = [];
	var col_joinedfeeds_selectedterm = db_cota_curio_fixedterm.collection(col_timestamp.toString());
	col_joinedfeeds_selectedterm.find().toArray(function (err, result) {
		if (err) {
			console.log(err);
		} else if (result.length){
			for (let i of result){
				var feature = i;
                		delete feature['_id'];
                		delete feature['bearing'];
                		delete feature['speed'];
                		delete feature['vehicle_label'];
                		delete feature['localtime'];
				delete feature['scheduled_arr'];
				delete feature['realtime_arr'];
				delete feature['stop_o'];
				delete feature['stop_d'];
				delete feature['start_date'];
				feature['longitude'] = feature['longitude'].toFixed(5);
				feature['latitude'] = feature['latitude'].toFixed(5);
				//feature['timegroup'] = Math.floor(i.timestamp/60)*60;
                		feeds.push(feature);
			};
			res.end(tsv.stringify(feeds));
		}
	});	
});

dispatcher.onGet("/cotafeeds_access", function(req, res) {
	res.writeHead(200, {"Content-Type": "text/plain"});
    	var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
        	|| req.connection.remoteAddress;
	console.log(ip);

	var ctimestamp;
	var qs = url.parse(req.url, true).query;
	if (qs.start != null){
		ctimestamp = parseInt(qs.start)/1000; //in sec
	} else {
		ctimestamp = (new Date().getTime() - 2*60*60*1000)/1000; //2 hours early in sec
	}

	var cdatetime = new Date(ctimestamp*1000);
	var cYear = cdatetime.getFullYear();
    	var cMonth = ("0" + (cdatetime.getMonth()+1)).slice(-2);
	var cDay = ("0" + cdatetime.getDate()).slice(-2);
	var cDate = cYear + cMonth + cDay;
	var cHour = cdatetime.getHours();
	var trip_category = null;
	if (cHour >= 8 && cHour < 13){
		trip_category = 1;
	} else if (cHour >= 13 && cHour < 18){
		trip_category = 2;
	} else {
		trip_category = 3;
	}
	
	var feeds = [];
	var col_cota_curio_dailytrips_selectedday = db_cota_curio_dailytrips.collection(cDate);
	col_cota_curio_dailytrips_selectedday.find({'trip_category':trip_category}).toArray(function (err, result) {
		if (err) {
			console.log(err);
		} else if (result.length){
			for (let i of result){
				
				for (let j of i['stop_time_update']){
					var feature = j;
					feature['trip_id'] = i['trip_id']
					feeds.push(feature);
				}
			};
			res.end(tsv.stringify(feeds));
		}
	});	
	
});

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});

